var express = require('express');
var router = express.Router();
var userDal = require('../model/users_dal');

router.get('/all', function(req, res) {
  userDal.GetAll(function (err, result) {
        if (err) throw err;
        res.render('users/displayAllUsers.ejs', {rs: result});
      }
  );
});



router.get('/location', function(req, res) {
    userDal.GetLocation(function (err, result) {
            if (err) throw err;
            res.render('users/displayLocation.ejs', {rs: result});
        }
    );
});

router.get('/games', function(req, res) {
    userDal.GetGames(function (err, result) {
            if (err) throw err;
            res.render('users/displayGames.ejs', {rs: result});
        }
    );
});

router.get('/genres', function(req, res) {
    userDal.GetGenres(function (err, result) {
            if (err) throw err;
            res.render('users/displayGenres.ejs', {rs: result});
        }
    );
});

router.get('/consoles', function(req, res) {
    userDal.GetConsoles(function (err, result) {
            if (err) throw err;
            res.render('users/displayConsoles.ejs', {rs: result});
        }
    );
});

router.get('/locationUsers', function (req, res) {
    userDal.GetLocationID(req.query.location_id, function (err, result) {
            if (err) throw err;

            res.render('users/displayLocationUsers.ejs', {rs: result, location_id: req.query.location_id});
        }
    );
});

router.get('/gameUsers', function (req, res) {
    userDal.GetGameID(req.query.game_id, function (err, result) {
            if (err) throw err;

            res.render('users/displayGamesUsers.ejs', {rs: result, game_id: req.query.game_id});
        }
    );
});

router.get('/genreUsers', function (req, res) {
    userDal.GetGenreID(req.query.genre_id, function (err, result) {
            if (err) throw err;

            res.render('users/displayGenresUsers.ejs', {rs: result, genre_id: req.query.genre_id});
        }
    );
});

router.get('/consoleUsers', function (req, res) {
    userDal.GetConsoleID(req.query.console_id, function (err, result) {
            if (err) throw err;

            res.render('users/displayConsolesUsers.ejs', {rs: result, console_id: req.query.console_id});
        }
    );
});




router.get('/', function (req, res) {
    userDal.GetByID(req.query.user_id, function (err, result1, result2, result3, result4) {
        if (err) throw err;
        res.render('users/displayUserInfo.ejs', {rs: result1, ga: result2, ge:result3, c:result4, user_id: req.query.user_id});
        }
    );
});



router.get('/create', function(req, res) {
    userDal.GetForSubmissionForm(req.query.location_id, function(err, result1, result2, result3, result4){
        if (err) throw err;
        res.render('users/userFormCreate.ejs', {rs: result1, rs2: result2, rs3: result3, rs4: result4 });
    })
});




router.post('/save', function(req, res) {
    console.log("username: " + req.query.username);
    console.log("first_name: " + req.query.first_name);
    console.log("last_name: " + req.query.last_name);
    console.log("email: " + req.query.email);
    console.log("password: " + req.query.password);
    console.log(req.query);


    userDal.Insert(req.query,
        function(err){
            if (err) {
                res.send(err);
        }
        else {
            res.render('users/newusersuccess.ejs', {rs: result, rs2: game_result, rs3: genre_result, rs4:console_result});
        }
    });

});

module.exports = router;
