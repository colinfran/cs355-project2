var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: "Colin's GamerTagGo Website", subtitle: 'Made for CS355' });
});

/* GET About page */
router.get('/about', function(req, res, next) {
  res.render('about', { title: 'cs355' });
});

router.get('/authenticate', function(req, res) {
  accountDAL.GetByEmail(req.query.email, req.query.password, function (err, account) {
    if (err) {
      res.send(err);
    }
    else if (account == null) {
      res.send("Account not found.");
    }
    else {
      res.send(account);
    }
  });
});

module.exports = router;
