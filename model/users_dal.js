var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.GetAll = function(callback) {
    connection.query('SELECT * FROM Users;',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
};

exports.GetLocation = function(callback) {
    connection.query('SELECT * FROM LocationName;',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
};

exports.GetGames = function(callback) {
    connection.query('SELECT * FROM Games;',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
};

exports.GetGenres = function(callback) {
    connection.query('SELECT * FROM Genre;',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
};

exports.GetConsoles = function(callback) {
    connection.query('SELECT * FROM Console;',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
};


exports.GetLocationID = function(location_id, callback) {
    console.log(location_id);
    var query = 'SELECT * FROM locationUserView where location_id=' + location_id;
    console.log(query);
    connection.query(query,
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            callback(false, result);
        }
    );
};

exports.GetGameID = function(game_id, callback) {
    console.log(game_id);
    var query = 'SELECT * FROM gameUserView where game_id=' + game_id;
    console.log(query);
    connection.query(query,
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            callback(false, result);
        }
    );
};

exports.GetGenreID = function(genre_id, callback) {
    console.log(genre_id);
    var query = 'SELECT * FROM genreUserView where genre_id=' + genre_id;
    console.log(query);
    connection.query(query,
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            callback(false, result);
        }
    );
};


exports.GetConsoleID = function(console_id, callback) {
    console.log(console_id);
    var query = 'SELECT * FROM consoleUserView where console_id=' + console_id;
    console.log(query);
    connection.query(query,
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            callback(false, result);
        }
    );
};





exports.GetByID = function(user_id, callback) {
    console.log(user_id);

    connection.query('SELECT * FROM UserView where user_id=' + user_id, function (err, result1) {
        connection.query('SELECT * FROM gameView where user_id=' + user_id, function (err, result2) {
            connection.query('SELECT * FROM genreView where user_id=' + user_id, function (err, result3) {
                connection.query('SELECT * FROM ConsoleView where user_id=' + user_id, function (err, result4) {
                    if(err) {
                        console.log(err);
                        callback(true);
                        return;
            }
            callback(false, result1, result2, result3, result4);
                })
            })
        })
    });
};


exports.GetForSubmissionForm = function(user_id, callback) {
    connection.query('SELECT l.location_id, l.location_name FROM Users u JOIN LocationName l ON u.location_id=l.location_id GROUP ' +
        'BY location_name;', function (err, result1) {
        connection.query('SELECT g.game_id, game_title FROM Games g JOIN FavGame f ON f.game_id=g.game_id GROUP ' +
            'BY game_title;', function (err, result2) {
            connection.query('SELECT g.genre_id, genre_title FROM Genre g JOIN FavGenre f ON f.genre_id=g.genre_id GROUP ' +
                'BY genre_title;', function (err, result3) {
                connection.query('SELECT c.console_id, console_name FROM Console c LEFT JOIN System s ON s.console_id=c.console_id GROUP ' +
                    'BY console_name;', function (err, result4) {
                        if(err) {
                            console.log(err);
                            callback(true);
                            return;
                        }
        callback(false, result1, result2, result3, result4);
    })
})})})};


/* NOTE: Just like functions in other languages the parameters sent to a function do not need to have the same names
 as the function definition. In this case, I'm sending the "req" parameter from the router to exports.Insert(),
 but exports.Insert() has defined this input parameter as "account_info"

 the callback parameter is an anonymous function passed like a parameter, that will be run later on within
 this function.
 */

exports.Insert = function(username, first_name, last_name, email, password, location_id, game_id, genre_id, console_id, callback) {
    var values = [username, first_name, last_name, email, password, location_id];
    connection.query('INSERT INTO Users (title, username, first_name, last_name, email, password, location_id) VALUES (?, ?)', values,
        function (err, result) {
            if (err == null && game_id != null && genre_id != null && console_id != null) {
                var Genre_qry_values = [];
                var Game_qry_values = [];
                var Console_qry_values = [];


                if (game_id instanceof Array) {
                    for (var i = 0; i < games.length; i++) {
                        Game_qry_values.push([result.insertId, game_id[i]]);
                    }
                }
                else {
                    Game_qry_values.push([result.insertId, games]);
                }

                if (genre_id instanceof Array) {
                    for (var i = 0; i < genre.length; i++) {
                        Genre_qry_values.push([result.insertId, genre_id[i]]);
                    }
                }
                else {
                    Genre_qry_values.push([result.insertId, genre_id]);
                }

                if (console_id instanceof Array) {
                    for (var i = 0; i < console.length; i++) {
                        Console_qry_values.push([result.insertId, console_id[i]]);
                    }
                }
                else {
                    Console_qry_values.push([result.insertId, console_id]);
                }
                console.log(Game_qry_values);
                console.log(Genre_qry_values);
                console.log(Console_qry_values);

                var queryGames = 'INSERT INTO FavGame (user_id, game_id) VALUES ?';
                var queryGenres = 'INSERT INTO FavGenre (user_id, genre_id) VALUES ?';
                var queryConsoles = 'INSERT INTO System (user_id, console_id) VALUES ?';
                connection.query(queryGames, [Game_qry_values], function (err, game_result) {
                    connection.query(queryGenres, [Genre_qry_values], function (err, genre_result) {
                        connection.query(queryConsoles, [Console_qry_values], function (err, console_result) {
                            if (err) {
                                Delete(result.insertId, function () {
                                    callback(err);
                                });
                                Delete(game_result.insertId, function () {
                                    callback(err);
                                });
                                Delete(genre_result.insertId, function () {
                                    callback(err);
                                });
                                Delete(console_result.insertId, function () {
                                    callback(err);
                                });
                            }
                            else {
                                callback(err);
                            }}
                        )}
                    )}
                );

            }
            else {
                callback(err);
            }

        }
    )
};






